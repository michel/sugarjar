require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  # repoconfig_spec requires a git repo
  # and in general isn't very applicable to ensuring
  # the resulting install is functional, so exclude it
  spec.exclude_pattern = './spec/repoconfig_spec.rb'
  spec.pattern = './spec/**/*_spec.rb'
end
